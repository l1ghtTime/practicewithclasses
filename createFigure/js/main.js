class Creator {
    constructor(selector){
        this.$el = document.createElement(selector);
        document.body.appendChild(this.$el);
    }
} 

const mix = {
    hide() {
        this.$el.style.display = 'none';
    },

    show() {
        this.$el.style.display = 'block';
    }
}

Object.assign(Creator.prototype, mix);

class Squre extends Creator{
    constructor(options) {
        super(options.selector);

        this.class = options.class;
        this.size = options.size;
        this.bg = options.bg;
    }

    create() {
        this.$el.classList.add(this.class);
        this.$el.style.width = this.$el.style.height = this.size + 'px';
        this.$el.style.backgroundColor = this.bg;
    }
}

const squre = new Squre({
    selector: 'div',
    class: '.squre',
    size: 100,
    bg: 'blue',
});


class Circle extends Squre{
    constructor(options) {
        super(options);

        this.radius = options.radius;
        this.$el.style.borderRadius = this.radius;
    }

}

const circle = new Circle({
    selector: 'div',
    class: '.squre',
    size: 100,
    bg: 'red',
    radius: '50%',
});

class Triangle extends Circle{
    constructor(options) {
        super(options);

        this.radiusSide = options.radiusSide;
        this.radiusBottom = options.radiusBottom;
    }

    create() {
        this.$el.classList.add(this.class);
        this.$el.style.width = this.$el.style.height = this.size + 'px';
        this.$el.style.borderLeft = this.$el.style.borderRight = this.radiusSide;
        this.$el.style.borderBottom = this.radiusBottom;
    }
}

const triangle = new Triangle({
    selector: 'div',
    class: '.triangle',
    size: 0,
    bg: 'green',
    radiusSide: '50px solid transparent',
    radiusBottom: '100px solid green',
});

