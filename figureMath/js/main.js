class Circle {
    constructor(radius) {
        this._radius = radius;
    }

    get radius() {
        return this._radius / (2 * Math.PI);
    }

    set radius(value) {
        this._radius = value;
    }

    get diametr() {
        return (this._radius / (2 * Math.PI)) * 2;
    }

    area() {
       return  Math.PI * (this._radius ** 2);
    }

    circleLength() {
        return 2 * Math.PI * this._radius;
    }

}

let circle = new Circle(10);


console.log(circle.radius);
console.log(circle.diametr);
console.log(circle.area());
console.log(circle.circleLength());