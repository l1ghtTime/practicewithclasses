class Marker{

    constructor(color, amounInk, selector) {
        this.color = color;
        this.amounInk = amounInk;
        this.selector = selector;
    }

    print() {
        this.selector.inputArea.style.color = this.color;
        
        let score = this.amounInk;

        this.selector.amount.innerHTML = score + ' %';
    
        this.selector.inputArea.addEventListener('input', () => {
            let reg = /\s|[a-z]+\s/ig;
            let newStr =  this.selector.inputArea .value.replace(reg, '');

            this.selector.inputArea.value = newStr;

            if(newStr) {
                score = score - 5;
                this.selector.amount.innerHTML = score + ' %';
            }

            if(score <= 0) {
                score = 0;
                this.selector.amount.innerHTML = score;
                this.selector.inputArea.setAttribute('disabled', '');
            }

        });
    }
}

const marker = new Marker('blue', 100, {
    inputArea: document.querySelector('#input'),
    amount: document.querySelector('.amount-ink')
});

marker.print();

class Ink extends Marker{
    full(){
        this.selector.amount.innerHTML = this.amounInk + ' %';
        this.selector.inputArea.removeAttribute('disabled');
    }
}

const ink = new Ink('', marker.amounInk,  marker.selector);





